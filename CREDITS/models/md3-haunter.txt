6 February 2001
================================================================
Model Name              : Haunter
installation directory  : quake3/baseq3/
Authors                 : Daylen "The Heretic" Stevens and Dexter Stevens
Email Address           : daylen@mweb.co.za

Model description       : Haunter, a ghost pokemon. It's our first model (my brother & me)
                          with 3dsmax, in fact it's not even our first, it's a basic (really basic,
                          hell, it doesn't even have any feet) model to teach ourselves 
                          the course of creating Quake III models.
                          We are well aware that it sucks (especially the skin) so you don't
                          need to tell us so.

================================================================
* Play Information *

New Sounds              : Only taunt.wav
CTF Skins               : yes
VWEP Support            : no
Bot Support             : borrowing Wrack's file


* Construction *
Poly Count              : 466
Vert Count              : ???
Skin Count              : 3
Base                    : New model
Editor used             : 3D Studio Max R3.1, Npherno's MD3 compile, Paint Shop Pro 6


* How to use this model *

unzip contents to your baseq3 folder
