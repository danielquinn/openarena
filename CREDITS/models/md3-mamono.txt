=======================================
   Mamono.md3
=======================================
By Will                              5/8/01

It's just a devil/demon character with jet boots.
This model is actually a boss character made for a
fighting game mod for UT that I've been doing. But 
since Mr. Steed put out those .bips I thought it would be
fun to stick him in Q3A. The animations are all straight 
from the Major.bip (I hope that's acceptable). 
I wanted him to use the Doom guys sounds instead of the
default Sarge ones but the only way I could figure out how
to do that was to include Doom's sound files in this .pk3. 
The skin was a huge learning experience for me as it was 
the first true color one I've done. I'm not very happy with
it but I did learn a lot. I hope someone can do him up 
a good skin set someday.(hint,hint;) 

I hope you enjoy this model!

Thanks to Paul Steed for putting out those .bip files and
for doing all those tutorials on looneygames.com those
taught me soooo much.   
---------------------------------------

E-Mail : will@bak.rr.com