<10th May 2001>
================================================================

Model Name            :	eelai
Installation Directory:	baseq3\md3-eelai.pk3
Model Author          :	Frag Cow
Skin Author           :	Frag Cow armed with 'ctrl c & v' (ie, copyed/paste from scan, see notes)

Email Address         :	fish_dsc@yahoo.co.uk
HTTP (no quake stuff) : www.arch.su.edu.au/~coen_d/
                        http://coend.www7.50megs.com/coend

Model description     :	6 hours, orig 1625 triangles, but reduced to 1500, (772 vert when all seams merged)

Additional Credits to :	Yukito Kishiro- concept/ texture, Steed- base animation (lucy.bip), Caryn "Hellchick" Law- sound.

Thanks to             : gundam@btinternet.com for tutorial at http://www.planetquake.com/polycount/resources/quake3/tutorials/MAX-to-q3data/md3q3data.shtml 

Authors notes         :	yet another anime girl, based on chacter Eelai, assistant to Profesor Nova, in the comic "Battle Angel Alita" (named "GUNN" japan).
 sound from Caryn "Hellchick" Law, hellchick@planetquake.com  http://www.planetquake.com/voxfeminae

================================================================
* Play Information *

Skins                 :	default , red, blue
Bot support           :	Yes
New Sounds            :	Yes 
CTF Skins             :	Yes 
Polycount             : 1500 + the 30 or so to cover the holes in cutting the model up for quake3
Known Bugs            :  I like modeling, i don't like animating, this includes vertex weighting to bones. so I tend to do a minimal job with the vertex weighting, relying on the vertex position in the model and knowledge of how physique in max works. (then get to scream when after texture mapping everything, applying physique and getting to see what happens when a vertex is placed just 'there' (as don't like to go back and change the model once in max))

* Construction *
Tools(s) used         :	(wrote own .tga to .lwo (geometry) converter in 'c' to get started/ porpotions) Lightwave 5.5 modeling, 3DS Max 4 + Character Studio, photoshop5.0, Q3Data (poor old 266 pentium)

* How to use this model *
<INSTALLATION INFO>
put md3-eelai.pk3 into your baseq3 folder.

* Copyright / Permissions *
I don't own the copywrite of Yukito Kishiro's intelectual property, or any other of the contributers to this work (well, other than perhaps my own, <frag cow>), so this is for personal enjoyment only. if you make money from this then you sux. Redistribute freely, hopefully intact.

QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.
"Battle Angel Alita" trade mark of Viz Communications "GUNN" first publish by SHUEISHA Inc. Tokyo 1992