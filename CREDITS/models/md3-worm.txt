07-12-00
================================================================
Model Name              : Worm
installation directory  : quake3/baseq3
Author                  : Turbo-J
Email Address           : megacrack@offcampus.net

Model description       : A worm from the famous game. A bit simply
			  but very cool. Enjoy it!



Additional Credits to   : id Software

================================================================
* Play Information *

New Sounds              : yes
CTF Skins               : yes


* Construction *
Poly Count              : 416
Vert Count              : 258
Skin Count              : 3
Editor used             : Modeled and animated in Quake Model Editor 			  and exported whit NPherno's md3 compiler
Build/Animation time    : 6 days


* How to use this model *

unzip model into baseq3 directory

* Copyright / Permissions *

QUAKE(R) and QUAKE III(R) are registered trademarks of id Software, Inc.



