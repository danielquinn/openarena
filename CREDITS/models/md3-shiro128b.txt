<4th May 2001>
================================================================

Model Name            :	shiro128b
Installation Directory:	baseq3\md3-shiro128b.pk3
Model Author          :	Frag Cow
Skin Author           :	Frag Cow armed with 'ctrl c & v'

Email Address         :	fish_dsc@yahoo.co.uk
HTTP (no quake stuff) : www.arch.su.edu.au/~coen_d/
                        http://coend.www7.50megs.com/coend

Model description     :	bit of fun in lightwave, about 5

Additional Credits to :	Matsumune Shiro- concept/ texture, Steed- base animation (major.bip), Caryn "Hellchick" Law- sound.

Thanks to             : gundam@btinternet.com for tutorial at http://www.planetquake.com/polycount/resources/quake3/tutorials/MAX-to-q3data/md3q3data.shtml 

Authors notes         :	yet another anime girl, based on picture "128" of matsumune shiro in intron depot 2. sound from Caryn "Hellchick" Law, hellchick@planetquake.com  http://www.planetquake.com/voxfeminae
md3-shiro128b.pk3 has several fixes that md3-shiro128.pk3 doesn't, jiggle (very importaint) bot and some sounds scraped together, and something to represent the character for the head in the preview plane.


================================================================
* Play Information *

Skins                 :	default , red, blue
Bot support           :	Yes
New Sounds            :	Yes 
CTF Skins             :	does red, blue count? 
Polycount             : 1413 (including 88 for matsamune shiro octapot inside head)
Known Bugs            : bit of a gap between torso and legs, and ahem, durring certain unlady like posses their is some black texture where black texture really should not be. (hey, it's Frag Cow quality work, shoot me) 

* Construction *
Tools(s) used         :	(wrote own .tga to .lwo converter in 'c' to get started/ porpotions) Lightwave 5.5 modeling, 3DS Max 4 + Character Studio, photoshop5.0, Q3Data

* How to use this model *
<INSTALLATION INFO>
put md3-shiro128b.pk3 into your baseq3 folder.

* Copyright / Permissions *
I don't own the copywrite of matsumune shiro intelectual property, so this is for personal enjoyment only. if you make money from this then you sux. Redistribute freely, hopefully intact.

QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.
