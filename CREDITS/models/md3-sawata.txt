
10-20-00
================================================================
Model Name              : Sawata
installation directory  : quake3arena/baseq3
Author                  : ZeroCartin
Skin Authors 		: KMan, ZeroCartin, FreestylaGTX, 
Email Address           : zero_cartin@hotmail.com
URL                     : http://www.planetquake.com/q3manga

Model description       : Here 'tis! My first complete model for Quake 3. Sawata is a character from a comic i made when I was a kid, and is now on our mod, Quake 3 Manga.


Other info              : Maybe you'll think it's a Sonic the Hedgehog copy, BUT IT AINT!! I made this character before this guy was born..

Additional Credits to   : Q3 Manga Staff, Q3M fans, VERY special thanx to KMan, Skullfire and FreestylaGTX
================================================================
* Play Information *

New Sounds              : Uses default sounds
CTF Skins               : Yes


* Construction *
Poly Count              : 992
Skin Count              : 4 or more---
Editor used             : 3DS Max 3.1(modelling), Milkshape 3d 1.4.1(animating and mapskin), Photoshop
Known Bugs              : Because of not having a gun in his hands, when in thirdperson and firing the lightning, see what happens
Build/Animation time    : Over like 4 days of only work in animation, but the whole process was like 3 months.. 


* How to use this model *

just unzip the md3-sawata.pk3 into your baseq3 directory and you're good to go.

* Copyright / Permissions *

QUAKE(R) and QUAKE II(R) are registered trademarks of id Software, Inc.