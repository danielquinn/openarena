<February 6, 2000>
================================================================
Model Name:		Dead Eye
Installation Directory:	baseq3\md3-deadeye.pk3
Model Author(s):	Wrath
Skin Author(s):		MrRogers, Wrath, and Agent
Shader Author(s):	Wrath, Agent
Email Address:		wrath@polycount.com, ryan@sunstorm.net, ook@sunstorm.net

Model description:	An undead gunslinger

Other info:		This is an original character design, I'm sure it's not the first skeletal cowboy/gunsligner ever created, but it's not based on a character I've seen before...so it's orginal.
			If you're intersted in doing addtional skins for this model...drop me a line, and I'll get you the stuff you'll need as well as any help I can offer.

Additional Credits to:	<id Software>

Thanks to:		MrRogers, for mapping and skinning the model.
			Agent, for coming up with the incredible "cine" skin and shader effects.
			NPherno for being such a great lay and making such damn useful tools.

Author(s) notes:	Wrath: Not much to say about this model...I hope everyone likes it and tells their friends and neighbors. If you do like it, be sure to take a bit of time to drop us a line and let us know...ecouraging words and compliments are always appreciated, and make great fuel for doing more player models.

================================================================
* Play Information *

Skins:			default, cine
Bot support:		No
New Sounds:		No
CTF Skins:		No

* Construction *
Who did what:		Wrath designed, modeled, animated Dead Eye...did most of the initial mapping, and skinned the revolver.
			MrRogers finished up the mapping and painted the default skins for the model.
			Agent came up with the really nifty idea for the "cine" skin...did the recoloration of MrRogers skins, and coded the shader effects.

Tools(s) used:		3DS Max 2.5, Character Studio, NST v.1, Corel Photopaint, Adobe Photoshop, notepad, NPherno's MD3 Compiler, and in house tools

* How to use this model *
<INSTALLATION INFO>
Unzip md3-deadeye.pk3 into the baseq3 folder.
In order to use this model as a player model, it may be necessary to create a baseq3/models/players/deadeye folder under the Quake3 directory, and place any file in it.
The model can also be used by entering "model deadeye" in the console..."model deadeye/cine" will use the "cine" skin

* Copyright / Permissions *
QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.

This model may be freely distributed in any form, UNALTERED.  Which means, you can't remove/change the readme file, other files, or add your own stuff to it and pass it along.
Copyright of the model and animations belong to me.  Copyright of the mapping and skin belong to MrRogers.  Copyright of the "cine" skin belongs to MrRogers and/or Agent.  I'm not sure which...they can fight it out.
Regardless...don't steal our stuff.  If you absolutely have to use it for something...have the decency to drop one of us an email and ask first.