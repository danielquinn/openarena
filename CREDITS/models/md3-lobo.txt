<12th July 2000>
================================================================

Model Name            :	Lobo
Installation Directory:	baseq3\md3-Lobo.pk3
Model Author          :	Firestarter
Skin Author           :	Firestarter

Email Address         :	ki11ingjoke@yahoo.com

Model description     :	`Ard bastard and all round cutey pie .

Additional Credits to :	Simon Bisley ,Keith Giffen ,Alan Grant (originators of this *version* of the Lobo character).Epsilon for the *voice* of Lobo .

Thanks to             : Sgt.Science for ChasStudio trick advice .Epsilon .PolyCount . 

Authors notes         :	Obvious ,cheap shot modeling but someone had to do it .If you dig the model ,or have some criticism drop me a line .If you create a skin for this model please send me a copy .
I`ve omitted the obligatory CTF skins in the hope that someone else will come up with something cool ,as all I can envisage now is Lobo in red leathers (indeed this is sitting on my HD),Lobo in denim ,you get the picture ,I can`t see the wood for the trees .

================================================================
* Play Information *

Skins                 :	default ,(base skins included)
Bot support           :	Yes
New Sounds            :	Yes (Epsilon voiced : plazarus@home.com) 
CTF Skins             :	nope ,intentionally omitted 
Polycount             : 915 (though there are a number of single-sided)
Known Bugs            : Quake 3 animation system

* Construction *
Tools(s) used         :	3DS Max 2.5 (modeling),3DS Max 3.0 + Character Studio (animation), NST v.1 + Benny Elmeks IOUV import/export + Philip Martins Q2MDL + Adobe Photoshop (skinning+skinmapping) ,Doktors Bot Studio (Bot Building) ,Q3Data (compiling) ,
Cool Edit96 (Wav editing) .

* How to use this model *
<INSTALLATION INFO>
Unzip q3mdl-lobo.zip into your baseq3 folder.
In order to use this model as a player model(ie it doesn`t show up in your model selection screen), it may be necessary to unzip this text file to your baseq3 directory USING FOLDER NAMES .(thanks for the tip Rube)

* Copyright / Permissions *
The Lobo character and likeness is copyright DC Comics Inc .Warner Bros Inc .
QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.

This zip file and all contained may be freely distributed , UNALTERED .Under no circumstances can it be used for any commercial purpose without express permission .

<28th January 2001>
================================================================

Model Name            :	Lobo
Skin Info             : Ctf Skins (Red`s Lobo /Blue`s Wolvie)
Installation Directory:	baseq3\skn-lobo-ctf.pk3
Skin Author           :	Firestarter

Email Address         :	firestarter@polycount.com
Web Address           : http://www.planetquake.com/polycount/cottages/hellhole

* How to use these skins *
<INSTALLATION INFO>
Unzip the zip to your Quake3 baseq3 directory .

* Copyright / Permissions *
The Lobo character and likeness is copyright DC Comics Inc .Warner Bros Inc .
QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.

This zip file and all contained may be freely distributed , UNALTERED .Under no circumstances can it be used for any commercial purpose without express permission .
