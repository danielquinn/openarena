<April 14, 2000>
================================================================

Model Name:		jbravo

Installation Directory:	baseq3\jbravo.pk3

Model Author:		James B. Gatzmer, aka _Goatman_

Skin Author:		_Goatman_

Address:		The_Goatman@usa.net

================================================================

Model description:
	Johny Bravo

Other info:
	This is my first model for any game.  It took me forever because I had to learn what the heck I was doing as I went along.  From dealing with low poly limitations, to expanding my knowledge of Character Studio, to learning the exporting tools necessary to get him into the game, I was completely lost.

Additional Credits:
	id Software, the Cartoon Network, and a bunch of Bravo fan sites where I found reference pictures and audio files.

Author(s) notes:
	First off, I know already a Johny Bravo put out by John "Ronin{TRIAD}" Fisher and Michael "Dragonne{TRIAD}" Conger for Quake 2.  And when I saw the one for Quake 3, I was bummed.  But I didn't want to just throw mine out.  I think that my take on the character varies enough to justify two versions.  He weighs in at 988 faces in separate pieces.  

Known issues:

Mesh stuff:
	Overall, he's a bit thin.  I misinterpreted how his joints might bend, so his elbows and hind end are a bit weird in extreme motions.

Skin stuff:
	I tried to find a happy medium between cartoonish and realistic with the skin, but I'm not very good at texturing organic stuff like muscles yet, so the skin leaves alot to be desired.  The lighter colors get washed out a bit in open lighted areas.

Animation:
	I used Steed's bitterman biped file as a base, but the only thing that survives from that is one of the death animations and the leg turning motion.  The proportions of Bravo alone, dictated new frames for almost the entire animation and the animation.cfg was mildy modified. There are a couple of jittery keys, like in the idle animation where the foot slides around a bit.  And finally, with some weapons the left hand is in place under the weapon and not in place on others... this seems to be a give and take situation, and instead of having the hand clip on larger weapons, I chose to have it float below the smaller ones.

Summary:
	Overall, I think I did okay for a first character, and I hope I can do much better in the future.  Criticism is welcome as long as it's rational and constructive and goes towards technical issues that I can improve upon in the future.  Otherwise, I think it's fairly obvious I know my limitations and plan to work on them.  Regardless, I think he's fun to watch run around and looks alright in game...

================================================================

Game Info

Skins:
	One default.

Bot support:
	Nope, until I figure out how to do bots or someone wants to help me.

New Sounds:
	Custom sounds are included... just grabbed off some fan sites and cleaned them up a bit.

CTF Skins:
	one red and on blue skin for the shirt but I have no idea if they work in CTF.


Software used:
	Modeled in Nichimen's Nendo, tweaked and animated in 3D Studio MAX 3.1 with Character Studio, exported with pop n' fresh's MAX plugin, and recompiled in Npherno's MD3 Compiler.  Photoshop was used for the textures, and SoundForge for the audio.

Installation:
	Just put the jbravo.pk3 file in your \baseq3 folder.  Don't unzip it to your baseq3 folder or you'll most likely get a shader error in the console.

Thanks:
	Thanks to id software, Steed for releasing his biped files, pop n fresh for his exporter, Npherno for his compiler, Wrath for some helpful info and his bounding box file, the folks in #model_design (especially Mort, Gwot, and III_Demon for their patience), and the folks at www.polycount.com, especially rogue13, for a wealth of great resources.

Groove is in the heart.

================================================================

* Copyright / Permissions *
	QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.


This model may be freely distributed in any form, UNALTERED, which means, you can't remove/change the readme file, other files, or add your own stuff to it and pass it along.