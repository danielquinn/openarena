<05/29/01>
================================================================
Model Name              : Clippit
Installation directory  : /baseq3/players/clippy
Author                  : Infested Maggot
Skins Author 		: B1ll
Sound Author		: Goatman
Email Address           : craig-young81@home.com

Model description       : Well this is basically Clippit the annoying paperclip from hell...for those unfamiliar just load up Microsoft Word. I took creative license with the hands since clippit is just a floating peice of metal. i recommend for just something more of a walking drone go with the clippit bot while "Clippit - Red" & "Clippit - Blue" are the more intelegent bots who can actually fight.

Additional Credits to   : id Software, Goatman for all the Assitance given, Microsoft for 			  creating this annoying little bugger

================================================================
* Play Information *

New Sounds              : Yes
CTF Skins               : Yes

* Construction *
LOD			: Nope
Poly Count              : 764 polys
Vert Count              : 405 Verts
Skin Count              : 3


* How to use this model *

drop the md3-clippit.pk3 into your baseq3 dir.

Known bugs		: Head disappears slightly in the HUD