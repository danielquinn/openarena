<February 6, 2000>
================================================================
Model Name:		Johnny Bravo
Installation Directory:	baseq3\bravo.pk3
Model Author(s):	John "Ronin{TRIAD}" Fisher
Skin Author(s):		John "Ronin{TRIAD}" Fisher, Michael                         "Dragonne{TRIAD}" Conger
Email Address:		jfi7056052@aol.com

Model description:	Hopefully you know who Johnny Bravo is.

Other info:		This is the rebuilt "from the ground up" Q3 version of my Johnny Bravo model. The more I look at my Q2 version, the more I gag as I think how the heck I let that out to the public. Gone is the "hip cocked to one side" stance, as well as all the problems with the mesh. 

Additional Credits to:	<id Software>, Cartoon Network

Thanks to:		Clan Triad, Abdul "War Destroyer" Brown
		

Author(s) notes:	Stand back everybody!!! Bravoman is here!!!

================================================================
* Play Information *

Skins:			4
Bot support:		No
New Sounds:		yes
CTF Skins:		yes

* Construction * 	Start to finish, total time I would say a week maybe. It's only a reconstruction.

Tools(s) used:		3DS Max 2.5, Character Studio, Adobe Photoshop, notepad, NPherno's MD3 Compiler, Q3tools, 

* How to use this model *
<INSTALLATION INFO>
Unzip md3-bravo.pk3 into the baseq3 folder.


* Copyright / Permissions *
QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.
Johnny Bravo is copyrighted and registered trademark of Cartoon Network 
