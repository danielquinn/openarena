Quake III Arena - Model

   Model Name:    Jack
   Bot Support:   No
   CTF colours:   No


Directions: 
Drop the md3-jack.pk3 file into your baseq3 directory.


------------------------------------------------------------------------

This is my first model, I thought a werewolf space-man would be origional, so here he is.  The modeling,
textures, and animation were done by me while taking a 'character animation for video games' class at the 
Academy of Art, San Francisco.  The shader was borrowed from Xaero and then altered.  Apparently the gauntlet
animation isnt't working, but who needs it anywayz.  Word to ya moms.


------------------------------------------------------------------------

thanks to:
Todd Gantzler, my teacher, and Manfred "Ultra-Monkey" Neber for pointing out that the run cycle "looks like
he's scampering."  Hopefully I've corrected that problem...

------------------------------------------------------------------------
Author:   Curtis Collins
E-Mail:   Nero5150@hotmail.com


------------------------------------------------------------------------


