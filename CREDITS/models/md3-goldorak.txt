February 20 2000
================================================================
Model Name              : Goldorak
installation directory  : baseq3
Author, Skin Author     : Alcor
Email Address           : jlbr@moncourrier.com
HomePage		: http://www.planetquake.com/polycount/cottages/alcor/

Shaders			: Guplik, guplik@pmail.net

Model description       : 
Goldorak is the main character of a tv show that was very popular in Japan, Europe and Qu�bec around 1985.  He's a big robot driven by a cool guy named Actarus.  Together they fight to save the earth, while the evil Vega army try to kill us all.  

So grab your Astero-Axe, your rocket launcher, your tag_torso... and let the blood rain you yellow horned war machine!

Other info  :  

Press the "Gesture" key to see the Astero-Axe in action!
I know it's playing fast, but Arena didn't want to let me play it slower...

================================================================
* Play Information *
New Sounds           : No
CTF Skins            : No

* Construction *

Editors used            : StudioMax3, CharacterStudio, Photoshop5, q3data, Md3Compile
known Bugs              : Some vertex behind the head aren't welded.


* How to use this model *
Extract Goldorak.pk3 into your baseq3 folder.

* Copyright / Permissions *QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.
This model isn't built in a lucrative goal, you must ask me for permission before using any parts of this model for another model...
