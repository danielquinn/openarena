01/16/2001 
================================================================ 
Model Name              : Homer Simpson (3d)
Author                  : DanT
Email Address           : dant@gmx.net

Model description       : This is a q3-model of Homer Simpson (3d) as seen an tv in Homer� with botfile.
			   
Thanks to               : IanW Tutorial 
================================================================ 
* Play Information * 

New Sounds              : YES (only german, i'm looking for quality english 'halloween - the shining')
CTF Skins               : YES
VWEP Support            : No 


* Construction * 
Poly Count              : 995
Vert Count              : 547
Skin Count              : original/team blue - red pants/team red 
Base                    : New model



* How to use this model * 

Just extract it into your baseq3 folder.

* Copyright / Permissions * 

Authors may not include this model in commercial products 
without prior written permission of DanT. 


