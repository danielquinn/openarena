8th March 2001
================================================================
Model Name              : Wraithguard
installation directory  : Quake iii Arena/baseq3/models/players/wraithguard
Author                  : Mark Laiman
Email Address           : wildrider@saimhann.freeserve.co.uk

Model description       : The Eldar (WH40k) each carry upon themselves a small gem called a waystone.
			In the event of death the Waystone captures the soul of the Eldar so that it 
			does not fall prey to the Chaos God Slaanesh.  In times of great need the 
			greatest of these souls are used to power the Eldar's warmachines.  Wraithlords
			and the smaller wraithguard.


Other info              : credit goes to Kenneth Scott for textures used (ends of limbs and pipes) 
			and to Paul Steed for animations (Keel's)


Additional Credits to   : Lots of Credit here goes to Id software for a FAB game.
			Games Workshop for the Great Imagery visit them at http://www.games-workshop.co.uk

Thanks to               : Thanks to The Wife for her patience :D
================================================================
* Play Information *

New Sounds              : no
CTF Skins               : yes



* Construction *
Poly Count              : 1291 polys
Vert Count              : 758 verts
Skin Count              : 5 Skins inc CTF
Base                    : New model from scratch
Editor used             : Milkshape 1.51 for construction, 3dStudio Max and Q3Data for conversion to md3.
			Adobe Photoshop 5.5 for the skins
Known Bugs              : None
Build/Animation time    : too feckin long


* How to use this model *

unzip the pk3 into your baseq3 directory

* Copyright / Permissions *

QUAKE III ARENA(R) is a registered trademark of id Software, Inc.
Warhammer 40,000(R) Eldar(R) and Wraithguard(R) are registered trademarks of Games Workshop plc.


Other Legal info about the use of your model 

You are free to distribute this model by any method as long as it remains in its UNALTERED original form and includes this text file.  You may not use it as a base for another model.  If you wish to skin it drop me a line wildrider@saimhann.freeserve.co.uk
