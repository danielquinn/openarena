January 6 2001
================================================================
Model Name              : Maximus
installation directory  : \baseq3
Author			: Bero
Email Address           : tomislav_bero@hotmail.com
================================================================

The general who became a slave
The slave who became a gladiator
The gladiator who become a hero

The great Roman General Maximus has once again led the legions to victory on the battlefield.
The war won, Maximus dreams of home, wanting only to return to his wife and son;
however, the dying Emperor Marcus Aurelius has one more duty for the general-to assume the mantle of his power.
Jealous of Maximus� favor with the emperor, the heir to the throne, Commodus , orders his execution and that of his family.
Barely escaping death, Maximus is forced into slavery and trained as a gladiator in the arena where his fame grows.
Now he has come to Rome, intent on avenging the murder of his wife and son by killing the new emperor Commodus.
Maximus has learned that the one power stronger than that of the emperor is the will of the people, and he knows he can only attain his revenge by becoming the greatest hero in all the empire.

================================================================
* Play Information *
New Sounds           	: Yes
CTF Skins            	: Yes
Bot Support 	     	: Yes
Lod Support 		: No
* Construction *
Poly Count              : 901
Vert Count              : 510
Skin Count              : 3 (two different, default and ctf)
Shader                  : 1 effect (skirt) 
Animation		: Paul Steed (Mix Q3A models: Razor, Sarge, Doom and Mynx)
Additional Credits to   : id Software
Thanks to               : Paul "Villam" Steed for great tutorials

* How to use this model *
Extract md3-Maximus.pk3 into your quake3\baseq3 folder.

* Copyright *QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.
* "GLADIATOR" & ALL RELATED MARKS & MEDIA ARE TM & � 2000 DREAMWORKS LLC & UNIVERSAL STUDIOS.

 