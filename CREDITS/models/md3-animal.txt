<April 15, 2000>
================================================================
Model Name:		Animal
Installation Directory:	baseq3\animal.pk3
Model Author(s):	John "Ronin{TRIAD}" Fisher
Skin Author(s):		John "Ronin{TRIAD}" Fisher
Email Address:		jfi7056052@aol.com

Model description:	Animal from the Muppets
		
Additional Credits to:	<id Software>, Jim Henson, 

Thanks to:		Clan Triad
		

Author(s) notes:	BEAT DRUMS!!!!

================================================================
* Play Information *

Skins:			4
Bot support:		No
New Sounds:		yes
CTF Skins:		yes

* Construction * 	About 2-3 weeks total time.
Tools(s) used:		3DS Max 2.5, Character Studio, Adobe Photoshop, notepad, NPherno's MD3 Compiler, Q3tools, 

* How to use this model *
<INSTALLATION INFO>
Unzip animal.zip into the baseq3 folder.


* Copyright / Permissions *
QUAKE(R) and QUAKE III Arena(R) are registered trademarks of id Software, Inc.
Animal(R), Muppets(R) are registered trademark and copyrighted Jim Henson Productions
