MAy 17, 2000
================================================================
Accessory's Name	: bot-bravo.pk3
Installation directory  : Quake III Arena/baseq3
Author                  : Chancellor Kirin
Email Address           : Kirinsensei@hotmail.com

Accessory's description	: Bot files for Ronin{Triad}'s Johnny Bravo model.


Other info              : Ronin{Triad} has granted permission for this bot file to be used as the officila bot file for the Johnny Bravo model.


================================================================
* Construction *
Base                    : new



* How to use this accessory *

place the pk3 file into the baseq3 folder

* Copyright / Permissions *

QUAKE(R) and QUAKE II(R) are registered trademarks of id Software, Inc.

 


