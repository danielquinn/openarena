# Credits


## The Game

OpenArena is a GPL project stemming from the work of the original Quake3.  The
people who worked on both projects are legion, but the development project can
be found [here](https://github.com/OpenArena/) if you're interested.


## Extra models & bots

The "extras" bundled with this repo are all collected from across the web, most
of which were pulled from [ModDB](https://moddb.com/), but some have just been
scrounged from various places over time.

Where it's been possible, the original author's README files have been retained
here in this folder.  If you have information about the original authors of
extras not already credited, please feel free to submit a merge request and
give credit where it's due.
