# Another Dockerised OpenArena Server

This is another attempt at repackaging OpenArena as a Docker image, but with
added functionality to make it easy to deploy & configure with environment
variables.


## Configuration

On its own, this will start just fine locally.  All you have to do is run
`docker-compose up`.  The container will build, and after a few minutes,
you'll have a running server.

The defaults may not be what you're looking for though, so if you want to
change them, you can set environment variables using a local
`docker-compose.override.yml` file to configure the server a few different
ways:

| Variable         | Default               | Notes                                                                                                                                                 |
| :--------------- | :-------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------- |
| `MOTD`           | Welcome               | Can be any string, but keep it under 42 characters.                                                                                                   |
| `PASSWORD`       | [a random string]     | This will allow remote control.  Probably best to leave this alone.                                                                                   |
| `MAX_CLIENTS`    | 16                    | The maximum number of players in an arena.                                                                                                            |
| `MAX_PING`       | 300                   | The maximum ping a player can have before they're rejected from joining the server.                                                                   |
| `ALLOW_DOWNLOAD` | 1 (enabled)           | Whether the server should allow clients to pull model data from it.                                                                                   |
| `DOWNLOAD_URL`   | http://localhost:8080 | The webserver where you want to direct users to download the models.                                                                                  |
| `BOT_MINPLAYERS` | 3                     | If there are fewer human players than this in a game, bots will be added to make up the difference.  As humans join however, the bots will be kicked. |
| `GAME_TYPE`      | 0 (Free for all)      | The type of game being played.  See the table below for more information.                                                                             |


### GAME_TYPE

The `GAME_TYPE` value can only be one of the following:

| Type              | Value |
| :---------------- | :---- |
| Free For All      | 0     |
| Tourney           | 1     |
| Team Deathmatch   | 3     |
| Capture the Flag  | 4     |
| Elimination       | 8     |
| CTF Elimination   | 9     |
| Last One Standing | 10    |
| Double Domination | 11    |
| Domination        | 12    |


### Example

For example, a common `docker-compose.override.yml` file might look like:

```shell
version: "2"
services:
  openarena:
    environment:
      - MOTD=Welcome to my fabulous server!
      - MAX_CLIENTS=12
      - DOWNLOAD_URL=https://my-website.com/path/to/a/folder/containing/baseoa
      - GAME_TYPE=4
```
